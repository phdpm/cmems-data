# load core functions
source("core/funcs.R")

# check if input name defined
if (!exists("nipo.fname")) 
  nipo.fname <- "input/reanalysis/bs-ulg-nut-rean-m_1629702556407.nc"

# plug-in libraries via lazyload 
library.lazyload(c("dplyr", "magrittr", "raster", "rasterVis", "lubridate"))
# get month names
m.names <- substr(month.name, 0, 3)

# work with nitrate dimension
ni.raster.1992 <- c()
ni.month.1992 <- c()
ni.raster.2017 <- c()
ni.month.2017 <- c()
for (i in 1:length(m.names)) {
  raster.1992 <- raster(nipo.fname, varname="no3", band=i)
  ni.raster.1992 %<>% append(raster.1992)
  ni.month.1992 %<>% append(paste(m.names[i], round(cellStats(raster.1992, "mean"), 2)))
  
  raster.2017 <- raster(nipo.fname, varname="no3", band=(25*12+i)) # 25 years from 1992 and current month as idx
  ni.raster.2017 %<>% append(raster.2017)
  ni.month.2017 %<>% append(paste(m.names[i], round(cellStats(raster.2017, "mean"), 2)))
}
# create stacked objects 
ni.stack.1992 <- stack(ni.raster.1992)
ni.stack.2017 <- stack(ni.raster.2017)

names(ni.stack.1992) <- ni.month.1992
names(ni.stack.2017) <- ni.month.2017

# calculate difference between this stacks
diffni.1992.2017 <- ni.stack.2017 - ni.stack.1992
names(diffni.1992.2017) <- m.names

# rasterize data to pdf, no way to make PNG ... 
cairo_pdf("output/pics/reanalysis/nit-pho/monthy-nitrate-1992.pdf", width = 11.69, height = 8.27, onefile = TRUE)
levelplot(x = ni.stack.1992, margin=F, at=seq(0,5,by=0.05), par.settings=BuRdTheme, contour=F)
dev.off()

cairo_pdf("output/pics/reanalysis/nit-pho/monthy-nitrate-2017.pdf", width = 11.69, height = 8.27, onefile = TRUE)
levelplot(x = ni.stack.2017, margin=F, at=seq(0,5,by=0.05), par.settings=BuRdTheme, contour=F)
dev.off()

cairo_pdf("output/pics/reanalysis/nit-pho/monthy-nitrate-diff-2017-vs-1992.pdf", width = 11.69, height = 8.27, onefile = TRUE)
levelplot(x = diffni.1992.2017, margin=F, at=seq(-2,2,by=0.05), par.settings=BuRdTheme)
dev.off()


po.raster.1992 <- c()
po.month.1992 <- c()
po.raster.2017 <- c()
po.month.2017 <- c()
for (i in 1:length(m.names)) {
  raster.1992 <- raster(nipo.fname, varname="po4", band=i)
  po.raster.1992 %<>% append(raster.1992)
  po.month.1992 %<>% append(paste(m.names[i], round(cellStats(raster.1992, mean), 2)))
  
  raster.2017 <- raster(nipo.fname, varname="po4", band=(25*12+i)) # 25 years from 1992 and current month as idx
  po.raster.2017 %<>% append(raster.2017)
  po.month.2017 %<>% append(paste(m.names[i], round(cellStats(raster.2017, mean), 2)))
}
# create stacked objects 
po.stack.1992 <- stack(po.raster.1992)
po.stack.2017 <- stack(po.raster.2017)

names(po.stack.1992) <- po.month.1992
names(po.stack.2017) <- po.month.2017

# calculate difference between this stacks
diffpo.1992.2017 <- po.stack.2017 - po.stack.1992
names(diffpo.1992.2017) <- m.names

# rasterize data to pdf, no way to make PNG ... 
cairo_pdf("output/pics/reanalysis/nit-pho/monthy-pho-1992.pdf", width = 11.69, height = 8.27, onefile = TRUE)
levelplot(x = po.stack.1992, margin=F, at=seq(0,5,by=0.05), par.settings=BuRdTheme, contour=F)
dev.off()

cairo_pdf("output/pics/reanalysis/nit-pho/monthy-pho-2017.pdf", width = 11.69, height = 8.27, onefile = TRUE)
levelplot(x = po.stack.2017, margin=F, at=seq(0,5,by=0.05), par.settings=BuRdTheme, contour=F)
dev.off()

cairo_pdf("output/pics/reanalysis/nit-pho/monthy-pho-diff-2017-vs-1992.pdf", width = 11.69, height = 8.27, onefile = TRUE)
levelplot(x = diffpo.1992.2017, margin=F, at=seq(-5, 5, by = 0.1), par.settings=BuRdTheme)
dev.off()
