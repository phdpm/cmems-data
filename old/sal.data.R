# load core functions
source("core/funcs.R")

# plug-in libraries via lazyload 
library.lazyload(c("ncdf4", "dplyr", "magrittr", "raster", "rasterVis", "lubridate", "rgdal"))

# specify input file name. if you download newest file - set your own
if (!exists("sal.fname")) 
  sal.fname <- "input/reanalysis/bs-cmcc-sal-rean-m_1629705847692.nc"


q <- readline("Do you want to process raw SALINITY REANALYSIS database .nc (yes) file or use old results (no)?: ")
if (tolower(q) %in% c("yes", "y")) {
  check.data()
  # connect to netCDF
  file <- nc_open(sal.fname)
  
  # get dimensions
  d.time <- ncvar_get(file, "time")
  d.lon <- ncvar_get(file, "lon")
  d.lat <- ncvar_get(file, "lat")
  
  # get lat;lon dataframe and check if point in bounds of poly
  lat <- c()
  lon <- c()
  for (tmp.lat in d.lat) {
    for (tmp.lon in d.lon) {
      lat <- c(lat, tmp.lat)
      lon <- c(lon, tmp.lon)
    }
  }
  # create lat;lon dataframe
  lat.lon.df <- data.frame(lat = lat, lon = lon)
  lat.lon.df$inbound <- point.in.bounds(lat.lon.df$lat, lat.lon.df$lon)
  
  # data[lon,lat,depth,time], if depth only 1 -> data[lon,lat,time]
  v.sal <- ncvar_get(file, "so")
  
  r <- list(mean = c(), sd = c(), max = c(), min = c(), n = c(), se = c())
  
  # iterate over time dimension
  for (sal.ti in 1:length(d.time)) {
    # iterate lat;lon
    sal.v <- c()
    for (i in 1:nrow(lat.lon.df)) {
      row <- lat.lon.df[i,]
      # do not process outliers
      if (row$inbound == 1) {
        v <- v.sal[row$lon, row$lat, sal.ti]
        if (!is.na(v)) sal.v %<>% append(v)
      }
    }
    
    # calculate statistics
    sal.mean <- mean(sal.v, is.na=FALSE)
    sal.sd <- sd(sal.v)
    
    # append results to global vectors
    r$mean %<>% append(sal.mean)
    r$sd %<>% append(sal.sd)
    r$min %<>% append(min(sal.v))
    r$max %<>% append(max(sal.v))
    r$n %<>% append(length(sal.v))
    r$se %<>% append(se(sal.v))
    
    # debug output
    print(sprintf("%s = %s", as.secondsToDate(d.time[sal.ti], start = "1970-01-01"), round(sal.mean, 2)))
    # debug breakpoint
    #if (sal.ti > 5) break
  }
  
  # save result as data.frame
  df.monthly <- data.frame(date = as.secondsToDate(d.time, start = "1970-01-01"), 
                           mean = r$mean, 
                           sd = r$sd,
                           min = r$min, 
                           max = r$max,
                           se = r$se,
                           n = r$n)
  
  #result$CV <- round(result$sd*100/result$mean, 2)
  # write result to .csv file
  write.csv(df.monthly, file = "output/data/reanalysis/mean.month.sal.csv")
  nc_close(file)
} else {
  if (!file.exists("output/data/reanalysis/mean.month.sal.csv")) stop("There is no output/data/reanalysis/mean.month.sal.csv data. Try to answer 'yes' to parse native ncdf file")
  # if output file exist - read from .csv data
  df.monthly <- read.csv("output/data/reanalysis/mean.month.sal.csv", header = TRUE)
}

# prepare summary yearly data
df.yearly <- df.monthly %>%
  mutate(year = lubridate::year(date)) %>%
  group_by(year) %>%
  summarise(smean = mean(mean), min = min(min), max = max(max), sd = sd(mean), se = se(mean))

# remove last year as "biased" (cuz only 1/2 of year data exist)
df.yearly <- df.yearly[-nrow(df.yearly),]

# write summarized data to csv
write.csv(df.yearly, file = "output/data/reanalysis/mean.year.sal.csv")

# plot reanalysis summarised 
plot.args <- list(x = df.yearly$year, y = df.yearly$smean, frame=F, type="b", xlab="Year", ylab = "Salinity")
save.plot("output/pics/reanalysis/sal.yearly.png", plot, plot.args)
